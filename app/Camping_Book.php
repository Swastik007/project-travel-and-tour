<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Camping_Book extends Model
{
    protected $table='camping_books';
    public function camping()
    {
        return $this->belongsTo('App\Camping');
    }
    public function camping_bookable()
    {
        return $this->morphTo();
    }

}
