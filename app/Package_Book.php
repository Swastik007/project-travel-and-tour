<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package_Book extends Model
{
    protected $table='package_books';
    public function package()
    {
        return $this->belongsTo('App\Package');
    }
    public function package_bookable()
    {
        return $this->morphTo();
    }
}
