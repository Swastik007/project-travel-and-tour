<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    public function hotel_books()
    {
        return $this->hasMany('App\Hotel_Book','hotel_id','hotel_id');
    }
    public function hotel_images()
    {
        return $this->hasMany('App\Hotel_Image');
    }

}
