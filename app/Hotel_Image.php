<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel_Image extends Model
{
    protected $table='hotel_images';
    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }
}
