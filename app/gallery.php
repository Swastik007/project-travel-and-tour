<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gallery extends Model
{
    protected $table= 'galleries';

    protected $primaryKey = 'gallery_id';

    protected $fillable= ['gallery_id','gallery_title','gallery_alias','gallery_image','gallery_status'];
}
