<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Camping_Image extends Model
{
    protected $table='camping_images';
    public function camping()
    {
        return $this->belongsTo('App\Camping');
    }
}
