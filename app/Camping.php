<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Camping extends Model
{
    public function camping_books()
    {
        return $this->hasMany('App\Camping_Book','camping_id','camping_id');
    }
    public function camping_images()
    {
        return $this->hasMany('App\Camping_Image','camping_id','camping_id');
    }
    public function camping_iternaries()
    {
        return $this->hasMany('App\Camping_Iternary','camping_id','camping_id');
    }
}
