<?php




Route::get('Hotel','HotelController@Index');
Route::group([], function () {   

Route::get('/testimonial', 'Home_Testimonial\Home_TestimonialController@index');

// Authentication routes...
Route::get('admin/login', 'Auth\AuthController@getLogin');
Route::post('admin/login', 'Auth\AuthController@postLogin');
Route::get('admin/logout', 'Auth\AuthController@getLogout');


Route::group(['middleware' =>['auth'], 'prefix' => 'admin'], function () {   

Route::get('/dashboard','Dashboard\DashboardController@render');

Route::resource('/menu','Menu\MenuController');

Route::resource('/gallery','Gallery\GalleriesController');

Route::resource('/page','Page\PageController');








});

});


