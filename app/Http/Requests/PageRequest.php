<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

            switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                 return [
                    'page_title'=>'required',

                    'page_alias'=>'required|unique:pages,page_alias',        
                                        
                    'page_status'=>'required'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'page_title'=>'required',
                    'page_alias'=>'required|unique:pages,page_alias,'.$this->segment(3).',page_id',
                    'page_status'=>'required'
                ];
            }
            default:break;
        }
    }
}