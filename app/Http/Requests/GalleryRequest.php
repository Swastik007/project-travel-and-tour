<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class GalleryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return[];
            }

            case 'POST':
            {
                return[
                'gallery_title' => 'required',
                'gallery_alias' => 'required',
                'gallery_image' => 'required',
                'gallery_status' => 'required'
                ];
            }

            case 'PUT':
            case 'PATCH':
            {
                return[
                'gallery_title' => 'required',
                'gallery_alias' => 'required',
                'gallery_image' => 'required',
                'gallery_status' => 'required'
                ];
            }

        }
        return [

            //
        ];
    }
}
