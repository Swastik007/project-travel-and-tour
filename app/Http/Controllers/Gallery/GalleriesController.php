<?php

namespace App\Http\Controllers\Gallery;

use Illuminate\Http\Request;

use App\Http\Requests\GalleryRequest;

use App\Http\Controllers\Controller;

use App\Repositories\GalleryRepository;

use Redirect;

use Auth;

use Exception;

use Session;

use Flash;

use App\Support\Helper;

class GalleriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response


     */
    function __construct(Request $request, GalleryRepository $galleryRepo){
            $this->galleryRepo=$galleryRepo;
            $this->request=$request;
        }

    public function index()
    {
        return View('galleries.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {

        return View('galleries.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GalleryRequest $request)
    {
      $data=$this->request->all();
      if($this->request->hasfile('gallery_image'))
      {
        $destinationPath=public_path('uploads\GalleryImages');
      
       $asd=$this->request->file('gallery_image')->move($destinationPath)->getFilename();
       dd($asd);
      }
      dd($data);
      $this->galleryRepo->create($data);
      flash::success('Successfully Created a gallery');
      return Redirect::to('/admin/gallery');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
