<?php

namespace App\Http\Controllers\Vehicle;

use Illuminate\Http\Request;
use App\Vehicle_Image;
use App\Support\Helper;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Vehicle;
use Flash;
use Redirect;
class VehicleController extends Controller
{
    private $vehicle;
    private $request;
    private $vehicleimage;
    public function __construct(Vehicle $vehicle, Request $request, Vehicle_Image $vehicleimage)
    {
        $this->vehicleimage=$vehicleimage;
        $this->request=$request;
        $this->vehicle=$vehicle;
    }
    public function create()
    {
        return View('Vehicle.form');
    }
    public function destroy($id)
    {

        $this->vehicle->where('vehicle_id',$id)->delete();
        Flash::success('SuccessFully deleted The vehicle');
        return Redirect::to('admin/vehicle');
    }
    public function store()
    {

        $this->vehicle->vehicle_Name=$this->request['vehicle_Name'];
        $this->vehicle->vehicle_Price=$this->request['vehicle_Price'];
        $this->vehicle->vehicle_Description=$this->request['vehicle_Description'];
        $this->vehicle->vehicle_Status=$this->request['vehicle_Status'];
        $this->vehicle->save();
        return Redirect::to('admin/vehicle');
    }
    public function addImage($id)
    {
        $images=$this->vehicleimage->where('vehicle_id',$id)->get();
        return View('Vehicle.addimage',compact('id','images'));
    }
    public function upload()
    {
        $this->vehicleimage->vehicle_id=$this->request['id'];
        $this->vehicleimage->vehicle_image_link= Helper::uploadImage($this->request->file('file'));
        $this->vehicleimage->save();

    }
    public function update($id)
    {

        $this->vehicle->where('vehicle_id',$id)->update(['vehicle_Name'=>$this->request['vehicle_Name'],
            'vehicle_Status'=>$this->request['vehicle_Status'],'vehicle_Price'=>$this->request['vehicle_Price'],
            'vehicle_Description'=>$this->request['vehicle_Description'],
         ]);

        Flash::success('SuccessFully Updated The vehicle');
        return Redirect::to('/admin/vehicle');
    }
    public function edit($id)
    {
        $vehicle=$this->vehicle->where('vehicle_id',$id)->first();
        return View('Vehicle.form',compact('vehicle'));
    }
    public function index()
    {
        $vehicles=$this->vehicle->with('vehicle_images')->get();
        return View('Vehicle.list',compact('vehicles'));
    }
    public function removeImage()
    {
        $name= $this->vehicleimage->where('vehicle_image_id',$this->request['id'])->first()->vehicle_image_link;
        Helper::removeImage($name);
        $this->vehicleimage->where('vehicle_image_id',$this->request['id'])->delete();
        Flash::success('SuccessFully deleted The Image');

    }
    public function changeStatus($id)
    {
        $m=$this->vehicle->where('vehicle_id',$id)->first();
        if($m->vehicle_Status=="Active")
            $this->vehicle->where('vehicle_id',$id)->update(['vehicle_Status'=>'Inactive']);
        else
            $this->vehicle->where('vehicle_id',$id)->update(['vehicle_Status'=>'Active']);
        return Redirect::to('/admin/vehicle');
    }
}
