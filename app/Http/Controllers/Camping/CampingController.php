<?php

namespace App\Http\Controllers\camping;

use App\Camping_Iternary;
use Illuminate\Http\Request;
use App\Support\Helper;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;
use Redirect;
use App\Camping;
use App\Camping_Image;
class campingController extends Controller
{
    private $camping;
    private $request;
    private $campingimage;
    private $campingiternary;
    public function __construct(Camping $camping, Request $request, Camping_Image $campingimage, Camping_Iternary $campingiternary)
    {
        $this->campingimage=$campingimage;
        $this->request=$request;
        $this->camping=$camping;
        $this->campingiternary=$campingiternary;
    }
    public function create()
    {
        return View('Camping.form');
    }
    public function destroy($id)
    {

        $this->camping->where('camping_id',$id)->delete();
        Flash::success('SuccessFully deleted The camping');
        return Redirect::to('admin/camping');
    }
    public function allIternary($id)
    {
        $iternaries=$this->campingiternary->where('camping_id',$id)->get();
        return $iternaries;
    }
    public function updateIternary($id)
    {
        $this->campingiternary->where('camping_id',$id)->delete();
        foreach ($this->request['iternaries'] as $i)
        {
            $c = new Camping_Iternary;
            $c->camping_id=$id;
            $c->camping_iternary_day=$i['camping_iternary_day'];
            $c->camping_iternary_description=$i['camping_iternary_description'];
            $c->save();
        }
    }
    public function store()
    {

        $this->camping->camping_Name=$this->request['camping_Name'];
        $this->camping->camping_Place=$this->request['camping_Place'];
        $this->camping->camping_Price=$this->request['camping_Price'];
        $this->camping->camping_Description=$this->request['camping_Description'];
        $this->camping->camping_Status=$this->request['camping_Status'];
        $this->camping->save();
        return Redirect::to('admin/camping');
    }
    public function addImage($id)
    {
        $images=$this->campingimage->where('camping_id',$id)->get();
        return View('Camping.addimage',compact('id','images'));
    }
    public function upload()
    {
        $this->campingimage->camping_id=$this->request['id'];
        $this->campingimage->camping_image_link= Helper::uploadImage($this->request->file('file'));
        $this->campingimage->save();

    }
    public function iternary($id)
    {
        return View('Camping.iternary',compact('id'));
    }
    public function update($id)
    {

        $this->camping->where('camping_id',$id)->update([
            'camping_Name'=>$this->request['camping_Name'],
            'camping_Place'=>$this->request['camping_Place'],
            'camping_Status'=>$this->request['camping_Status'],
            'camping_Price'=>$this->request['camping_Price'],
            'camping_Description'=>$this->request['camping_Description'],
        ]);

        Flash::success('SuccessFully Updated The camping');
        return Redirect::to('/admin/camping');
    }
    public function edit($id)
    {
        $camping=$this->camping->where('camping_id',$id)->first();
        return View('Camping.form',compact('camping'));
    }
    public function index()
    {
        $campings=$this->camping->with('camping_images')->get();
        return View('Camping.list',compact('campings'));
    }
    public function removeImage()
    {
        $name= $this->campingimage->where('camping_image_id',$this->request['id'])->first()->camping_image_link;
        Helper::removeImage($name);
        $this->campingimage->where('camping_image_id',$this->request['id'])->delete();
        Flash::success('SuccessFully deleted The Image');

    }
    public function changeStatus($id)
    {
        $m=$this->camping->where('camping_id',$id)->first();
        if($m->camping_Status=="Active")
            $this->camping->where('camping_id',$id)->update(['camping_Status'=>'Inactive']);
        else
            $this->camping->where('camping_id',$id)->update(['camping_Status'=>'Active']);
        return Redirect::to('/admin/camping');
    }
}
