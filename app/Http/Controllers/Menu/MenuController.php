<?php

namespace App\Http\Controllers\Menu;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Menu;
use Redirect;
use Flash;
use App\Page;
class MenuController extends Controller
{
    private $menu;
    private $request;
    private $page;
    public function __construct(Menu $menu, Request $request, Page $page)
    {
        $this->page=$page;
        $this->request=$request;
        $this->menu=$menu;
    }

    public function index()
    {

        $menus=$this->menu->all();
        return View('Menu.list',compact('menus'));
    }
    public function show($id)
    {
       return $this->menu->where('menu_id',$id)->first();
        $menu=$this->menu->where('menu_id',$id)->first();
        return View('Menu.form',compact('menu'));
    }
    public function edit($id)
    {

        $page=$this->page->lists('page_title','page_id');

        $pages=$this->page->all();
        $menu=$this->menu->where('menu_id',$id)->first();
        return View('Menu.form',compact('menu','pages','page'));
    }
    public function destroy($id)
    {

        $m= $this->menu->where('menu_id',$id)->delete();
        Flash::success('SuccessFully Updated The Hotel');
        return Redirect::to('admin/menu');
    }
    public function changeStatus($id)
    {

        $m=$this->menu->where('menu_id',$id)->first();
        if($m->menu_status=="active")
            $this->menu->where('menu_id',$id)->update(['menu_status'=>'dormant']);
        else
            $this->menu->where('menu_id',$id)->update(['menu_status'=>'active']);
        return Redirect::to('/admin/menu');
    }
    public function update($id)
    {
        if($this->request['menu_type']=='page')
        {
            $this->menu->where('menu_id',$id)->update(['menu_title'=>$this->request['menu_title'],
                'menu_type_id'=>$this->request['menu_type_id'],'menu_type'=>$this->request['menu_type']]);

        }
        $this->menu->where('menu_id',$id)->update(['menu_title'=>$this->request['menu_title'],
        'menu_guid'=>$this->request['menu_guid'],'menu_type'=>$this->request['menu_type']]);


        Flash::success('SuccessFully Updated The Hotel');
        return Redirect::to('/admin/menu');
    }
    public function store()
    {

        if($this->request['menu_type']=='page')
        {
            $this->menu->menu_title = $this->request['menu_title'];
            $this->menu->menu_type_id = $this->request['menu_type_id'];
            $this->menu->menu_type = $this->request['menu_type'];
            $this->menu->menu_status = 'dormant';
        }
        else{
            $this->menu->menu_title = $this->request['menu_title'];
            $this->menu->menu_guid = $this->request['menu_guid'];
            $this->menu->menu_type = $this->request['menu_type'];
            $this->menu->menu_status = 'dormant';
        }
        $this->menu->save();
        return Redirect::to('/admin/menu');

    }
    public function create()
    {
        $page=$this->page->lists('page_title','page_id');

        return View('Menu.form',compact('page'));
    }
}
