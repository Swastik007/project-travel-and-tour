<?php
namespace App\Http\Controllers\Flight;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\flight;
use Flash;
use Redirect;
use App\Support\Helper;
class FlightController extends Controller
{
    private $flight;
    private $request;
    private $flightimage;
    public function __construct(Flight $flight, Request $request)
    {
        $this->request=$request;
        $this->flight=$flight;
    }
    public function create()
    {
        return View('Flight.form');
    }
    public function destroy($id)
    {

        $this->flight->where('flight_id',$id)->delete();
        Flash::success('SuccessFully deleted The flight');
        return Redirect::to('admin/flight');
    }
    public function store()
    {

        $this->flight->flight_Name=$this->request['flight_Name'];
        $this->flight->flight_Place=$this->request['flight_Place'];
        $this->flight->flight_Location=$this->request['flight_Location'];
        $this->flight->flight_Price=$this->request['flight_Price'];
        $this->flight->flight_Description=$this->request['flight_Description'];
        $this->flight->flight_Status=$this->request['flight_Status'];
        $this->flight->flight_Rating=$this->request['star'];
        $this->flight->flight_Deals='Coming Soon..';
        $this->flight->save();
        return Redirect::to('admin/flight');
    }
    public function upload()
    {
        $this->flightimage->flight_id=$this->request['id'];
        $this->flightimage->flight_image_link= Helper::uploadImage($this->request->file('file'));
        $this->flightimage->save();

    }
    public function update($id)
    {

        $this->flight->where('flight_id',$id)->update(['flight_Name'=>$this->request['flight_Name'],
            'flight_Place'=>$this->request['flight_Place'],'flight_Location'=>$this->request['flight_Location'],
            'flight_Status'=>$this->request['flight_Status'],'flight_Price'=>$this->request['flight_Price'],
            'flight_Description'=>$this->request['flight_Description'],
            'flight_Rating'=>$this->request['star']
        ]);

        Flash::success('SuccessFully Updated The flight');
        return Redirect::to('/admin/flight');
    }
    public function edit($id)
    {
        $flight=$this->flight->where('flight_id',$id)->first();
        return View('Flight.form',compact('flight'));
    }
    public function index()
    {
        $flights=$this->flight->with('flight_images')->get();
        return View('Flight.list',compact('flights'));
    }

    public function changeStatus($id)
    {
        $m=$this->flight->where('flight_id',$id)->first();
        if($m->flight_Status=="Active")
            $this->flight->where('flight_id',$id)->update(['flight_Status'=>'Inactive']);
        else
            $this->flight->where('flight_id',$id)->update(['flight_Status'=>'Active']);
        return Redirect::to('/admin/flight');
    }
}
