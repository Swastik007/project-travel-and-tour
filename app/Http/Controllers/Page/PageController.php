<?php

namespace App\Http\Controllers\Page;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Page;
use App\Support\Helper;
use Redirect;
use Flash;
use App\Menu;
class PageController extends Controller
{
    private $page;
    private $menu;
    private $request;

    public function create()
    {
        return View('page.form');
    }
    public function edit($id)
    {
        $page=$this->page->where('page_id',$id)->first();
        return View('Page.form',compact('page'));
    }
    public function store()
    {

        $this->page->page_title=$this->request['page_title'];
        $this->page->page_alias=$this->request['page_alias'];
        $this->page->page_image=Helper::uploadImage($this->request->file('page_image'));
        $this->page->page_content=$this->request['page_content'];
        $this->page->page_status=$this->request['page_status'];
        $this->page->save();
        return Redirect::to('admin/page');
    }
    public function __construct(Page $page, Request $request, Menu $menu)
    {
        $this->menu=$menu;
        $this->request=$request;
        $this->page=$page;
    }
    public function destroy($id)
    {

       $this->page->where('page_id',$id)->delete();
        Flash::success('SuccessFully Updated The Hotel');
        return Redirect::to('admin/page');
    }
    public function index()
    {
        $pages=$this->page->all();
        return View('page.list',compact('pages'));
    }
    public function update($id)
    {

        $this->page->where('page_id',$id)->update(['page_title'=>$this->request['page_title'],
            'page_alias'=>$this->request['page_alias'],'page_content'=>$this->request['page_content'],'page_status'=>$this->request['page_status'],'page_image'=>'page_image']);

        Flash::success('SuccessFully Updated The Hotel');
        return Redirect::to('/admin/page');
    }
    public function changeStatus($id)
    {

        $m=$this->page->where('page_id',$id)->first();
        if($m->page_status=="active")
            $this->page->where('page_id',$id)->update(['page_status'=>'dormant']);
        else
            $this->page->where('page_id',$id)->update(['page_status'=>'active']);
        return Redirect::to('/admin/page');
    }
}
