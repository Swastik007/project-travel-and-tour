<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    public function package_books()
    {
        return $this->hasMany('App\Package_Book','package_id','package_id');
    }
    public function package_images()
    {
        return $this->hasMany('App\Package_Image','package_id','package_id');
    }
    public function package_iternaries()
    {
        return $this->hasMany('App\Package_Iternary','package_id','package_id');
    }
}
