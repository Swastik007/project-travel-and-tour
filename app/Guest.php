<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    public function activities()
    {
        return $this->morphMany('App\Activity_Book', 'activity_bookable');
    }
    public function campings()
    {
        return $this->morphMany('App\Camping_Book', 'camping_bookable');
    }
    public function flights()
    {
        return $this->morphMany('App\Flight_Book', 'flight_bookable');
    }
    public function hotels()
    {
        return $this->morphMany('App\Hotel', 'hotel_bookable');
    }
    public function packages()
    {
        return $this->morphMany('App\Package', 'package_bookable');
    }
    public function vehicles()
    {
        return $this->morphMany('App\Vehicle', 'vehicle_bookable');
    }
}
