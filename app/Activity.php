<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    public function activity_books()
    {
        return $this->hasMany('App\Activity_Book');
    }
    public function activity_images()
    {
        return $this->hasMany('App\Activity_Image');
    }
    
}
