<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package_Iternary extends Model
{
    protected $table='package_iternaries';
    public function package()
    {
        return $this->belongsTo('App\Package');
    }
    
}
