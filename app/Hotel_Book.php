<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel_Book extends Model
{
    protected $table='hotel_books';
    public function hotel()
    {
        return $this->belongsTo('App\Hotel','hotel_id','hotel_id');
    }
    public function hotel_bookable()
    {
        return $this->morphTo();
    }

}
