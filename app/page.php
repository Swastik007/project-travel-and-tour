<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

    protected $table = 'pages';

    protected $primaryKey = 'page_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_id', 'page_title', 'page_alias', 'page_image', 'page_content','page_status','page_created_by'
    ];


}
