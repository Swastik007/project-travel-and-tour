<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    public function vehicle_books()
    {
        return $this->hasMany('App\Vehicle_Book');
    }
    public function vehicle_images()
    {
        return $this->hasMany('App\Vehicle_Image');
    }
}
