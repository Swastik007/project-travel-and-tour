<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Camping_Iternary extends Model
{
    protected $table='camping_iternaries';
    public function camping()
    {
        return $this->belongsTo('App\Camping');
    }
}
