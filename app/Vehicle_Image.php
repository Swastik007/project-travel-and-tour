<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle_Image extends Model
{
    protected $table='vehicle_images';
    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }
}
