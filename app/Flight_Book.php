<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight_Book extends Model
{
    public function flight()
    {
        return $this->belongsTo('App\Flight');
    }
    public function flight_bookable()
    {
        return $this->morphTo();
    }
}
