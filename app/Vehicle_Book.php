<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle_Book extends Model
{
    protected $table='vehicle_books';
    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }
    public function vehicle_bookable()
    {
        return $this->morphTo();
    }
}
