<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    public function flight_books()
    {
        return $this->hasMany('App\Flight_Book');
    }
    public function flight_images()
    {
        return $this->hasMany('App\Flight_Image');
    }
}
