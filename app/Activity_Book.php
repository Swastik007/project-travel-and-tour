<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity_Book extends Model
{
    protected $table='activity_books';
    public function activity()
    {
        return $this->belongsTo('App\Activity');
    }
    public function activity_bookable()
    {
        return $this->morphTo();
    }
}
