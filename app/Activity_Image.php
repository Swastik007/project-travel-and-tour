<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity_Image extends Model
{
    protected $table='activity_images';
    public function activity()
    {
        return $this->belongsTo('App\Activity');
    }

}
