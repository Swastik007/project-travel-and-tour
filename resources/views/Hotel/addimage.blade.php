@extends('admin')

@section('admin-css')
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/custom.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/ratings.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/theme-default/libs/dropzone/dropzone.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/theme-default/libs/dropzone/material.css') }}" />

    <style type="text/css">
        .thumbnail {
            border: 0 none;
            box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.12);
        }
        .blur {
            position:relative;
        }

        /* Static Image Shown Before Hover */
        .blur a.static {
            position:absolute;
            left:0;
            top:0;
            z-index:1;
        }

        /* Image Shown Upon Hover */
        .blur a.blur {
            position:absolute;
            left:0;
            top:0;
        }
        a:hover{
            text-decoration: none;
        }
    </style>
@stop
@section('body')
    @include('partials.admin.validation')
    @include('partials.admin.flash')

    <section class="content-header">
        <h1>
          Images:
        </h1>

        <ol class="breadcrumb">
            <a href="{{ URL::to('/admin/hotel') }}">
                <button type="button" class="btn btn-block btn-primary btn-flat">Back</button>
                <a>
        </ol>

    </section>
    <br>
    <div class="card">
        <div class="card-body">
            <!-- BEGIN TABLE HOVER -->
            <section class="style-default-bright">
                <div class="section-body">
                    <form  action="{{ url('image/do-upload') }}" class="dropzone" id="addImages">

                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$id}}">
                    </form>
                </div>
            </section>

        </div><!--end .card-body -->
    </div><!--end .card -->
    <div class="card">
        <div class="card-body">
            <!-- BEGIN TABLE HOVER -->
            <section class="style-default-bright">
                <div class="section-header">
                    <h3>Uploaded Images:</h3>
                </div>
                <div class="section-body">
                    <div class="row">
                        @if(isset($images))
                            @foreach($images as $i)
                                <div class="col-xs-6 col-md-3 blur" data-galleryImageDiv="{{ $i->hotel_image_id }}">
                                    <a href="#" class="thumbnail" class="static">
                                        <img width="250px" src="{{ asset('/uploads').'/'.$i->hotel_image_link}}" alt="hotelimage" >
                                    </a>

                                    <a href="#" data-galleryImageId="{{ $i->hotel_image_id }}" class="blur removeImage">
                                        <span class="label label-danger">Remove</span>

                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </section>
        </div><!--end .card-body -->
    </div><!--end .card -->
@stop
@section('admin-js')
    <script type="text/javascript" src="{{asset('admin-assets/js/libs/dropzone/dropzone.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            var uploadFilePath = "{{ URL::to('/admin/hotel/upload') }}";
            var myDropzone     = new Dropzone("form#addImages", {
                url: uploadFilePath,
                maxFilesize: "5",
                init: function () {
                    this.on('complete', function () {
                        if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                            swal("Success!", "Upload Complete", "success")
                            location.reload();
                        }
                    });
                }
            });
        });
        $(document).ready(function(){
            $('.removeImage').on('click',function(event){
                event.preventDefault();
                var imageId = $(this).attr('data-galleryImageId');
                console.log('Image_id: '+imageId);
                $.ajax({
                    url:"{{ URL::to('/admin/hotel/removeImage') }}",
                    type: "POST",
                    data: {"_token": "{{ csrf_token() }}","id":imageId},
                    success: function(data)
                    {
                        location.reload();
                        data = JSON.parse(data);
                        console.log(data);
                        if(data.result == 'true')
                        {
                            swal(data.message);
                            $('div[data-galleryImageDiv='+imageId+']').remove();
                        }
                        else
                        {
                            swal(data.message);
                        }
                    },
                    error: function (data)
                    {
                        swal("Something Went Wrong.Please Try Again.");
                    }
                });
            });
        });

    </script>
    @stop