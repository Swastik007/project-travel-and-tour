@extends('admin')

@section('admin-css')
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/custom.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/ratings.css') }}" />

    <style type="text/css">
        .hide{
            display: none;
        }
    </style>
@stop
@section('body')
    <section class="content-header">
        <h1>
            Add New Hotel
        </h1>
        <ol class="breadcrumb">
            <a href="{{ URL::to('/admin/hotel') }}">
                <button type="button" class="btn btn-block btn-primary btn-flat">All Hotels</button>
                <a>
        </ol>
    </section>
    <br>
    @include('partials.admin.validation')
    @include('partials.admin.flash')

    <section>
        <div class="box">
            <div class="box-body">
                @if(Request::segment('4') == 'edit')
                    {!! Form::model($hotel,
                      ['url' => 'admin/hotel'.'/'.$hotel->hotel_id, 'class'=>'form-horizontal','method'=>'PATCH','files'=>'true','novalidate'=>'true'])!!}
                @else
                    {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/hotel', 'class'=>'form-horizontal','files'=>'true','novalidate'=>'true'])!!}
                @endif

                <div class="form-group">
                    <div class="col-sm-2">
                        {{ Form::label('hotel_Name', ' Name',['class'=>'col-sm-2 control-label strongLabel']) }}
                    </div>
                    <div class="col-sm-10">
                        {!! Form::text('hotel_Name',null,['class'=>'form-control','id'=>'hotel_Name','required'=>'true']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2">
                        {{ Form::label('hotel_Place', 'Place',['class'=>'col-sm-2 control-label strongLabel']) }}
                    </div>
                    <div class="col-sm-10">
                        {{form::text('hotel_Place',null,['class' =>'form-control','id'=>'hotel_Place'])}}
                    </div>
                </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                            {{ Form::label('hotel_Location', 'Location',['class'=>'col-sm-2 control-label strongLabel']) }}
                        </div>
                        <div class="col-sm-10">
                            {{form::text('hotel_Location',null,['class' =>'form-control','id'=>'hotel_Location'])}}
                        </div>
                    </div>
                    <div class="form-group">
                    <div class="col-sm-2">
                        {{ Form::label('hotel_Description', 'Content',['class'=>'col-sm-2 control-label strongLabel']) }}
                    </div>
                    <div class="col-sm-10">
                        {!! Form::textarea('hotel_Description',null,['id'=>'summernote','required'=>'true']) !!}
                    </div>
                </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                            {{ Form::label('hotel_Price', 'Price (NRP)',['class'=>'col-sm-2 control-label strongLabel']) }}
                        </div>
                        <div class="col-sm-10">
                            {{form::number('hotel_Price',null,['class' =>'form-control','id'=>'hotel_Price'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                            {{ Form::label('hotel_Rating', 'Rating',['class'=>'col-sm-2 control-label strongLabel']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="stars">
                                    <input class="star star-5" id="star-5" type="radio" value="5" name="star"/>
                                    <label class="star star-5" for="star-5"></label>
                                    <input class="star star-4" id="star-4" type="radio" value="4" name="star"/>
                                    <label class="star star-4" for="star-4"></label>
                                    <input class="star star-3" id="star-3" type="radio" value="3" name="star"/>
                                    <label class="star star-3" for="star-3"></label>
                                    <input class="star star-2" id="star-2" type="radio" value="2" name="star"/>
                                    <label class="star star-2" for="star-2"></label>
                                    <input class="star star-1" id="star-1" type="radio" value="1" name="star"/>
                                    <label class="star star-1" for="star-1"></label>
                            </div>
                        </div>
                    </div>
                <div class="form-group">
                    <div class="col-sm-2">
                        {{ Form::label('hotel_Status', 'hotel Status',['class'=>'col-sm-2 control-label strongLabel']) }}
                    </div>
                    <div class="col-sm-10">
                        {!! Form::select('hotel_Status',['active'=>'active','dormant'=>'dormant'],null,['class'=>'form-control','required'=>'true']) !!}
                    </div>
                </div>



                <div class="card-actionbar">
                    <div class="card-actionbar-row">
                        {!! Form::submit('Submit',['class'=>'btn btn-flat btn-primary']) !!}
                    </div>
                </div>


                {!! Form::close() !!}
            </div><!--end .card-body -->
        </div><!--end .card -->
    </section>
    <script type="text/javascript">
        $(document).ready(function(){

        });
        var qw=0;
        function addDiv(input) {
            if(qw==undefined)
                    qw=0;
            else
                    qw++;
            console.log(qw);
            $('#images').append('<img alt="img" id="image_display' +qw + '"></img>');

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#image_display'+qw).attr('src', e.target.result).width(150).height(200);
                   // console.log(e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }


        }
    </script>
@stop