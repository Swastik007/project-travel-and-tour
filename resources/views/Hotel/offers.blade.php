@extends('admin')

@section('admin-css')
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/custom.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/ratings.css') }}" />

    <style type="text/css">
        .hide{
            display: none;
        }
    </style>
@stop
@section('body')

    <div class="well">
        <div class="jumbotron">
            {!! Form::model($hotel,
                       ['url' => 'admin/hotel/updateOffer'.'/'.$hotel->hotel_id, 'class'=>'form-horizontal','method'=>'PATCH','files'=>'true','novalidate'=>'true'])!!}

            <div class="form-group">
               <h3>Offers:</h3>
                {!! Form::textarea('hotel_Deals',null,['class'=>'form-control','id'=>'hotel_Deals','required'=>'true']) !!}

            </div>
            <div class="card-actionbar-row">
                {!! Form::submit('Submit',['class'=>'btn btn-flat btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@stop