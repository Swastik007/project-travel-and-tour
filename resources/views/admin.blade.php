@include('partials.admin.header')
@include('partials.admin.menu')
@yield('body')
@include('partials.admin.footer')
