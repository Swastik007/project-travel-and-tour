
@extends('admin')

@section('admin-css')
 <link href="{{ asset("/bower_components/AdminLte/dist/css/summernote/summernote.css")}}" rel="stylesheet" type="text/css" />

@stop
@section('body')
<!-- BEGIN CONTENT-->

 <section class="content-header">
      <h1>
       Add New Page
       
      </h1>
       <ol class="breadcrumb">
      <a href="{{ URL::to('/admin/page') }}">
        <button type="button" class="btn btn-block btn-primary btn-flat">All Pages</button>
       <a>
      </ol>
     
    </section>
    <br>
    <section class="content">
@include('partials.admin.validation')
@include('partials.admin.flash')

<div class="box">
<div class="box-body">
@if(Request::segment('4') == 'edit')
{!! Form::model($page,
  ['url' => 'admin/page'.'/'.$page->page_id, 'class'=>'form-horizontal','method'=>'PATCH','files'=>'true','novalidate'=>'true'])!!}
@else
{!! Form::open(['url' => 'admin/page', 'class'=>'form-horizontal','files'=>'true','novalidate'=>'true'])!!}
@endif

  <div class="form-group">
    <div class="col-sm-2">
    {{ Form::label('page_title', ' Title',['class'=>'col-sm-2 control-label strongLabel']) }}
    </div>
    <div class="col-sm-10">
      {!! Form::text('page_title',null,['class'=>'form-control','id'=>'page_title','required'=>'true']) !!}
    </div>
  </div>  
  <div class="form-group">
    <div class="col-sm-2">
    {{ Form::label('page_alias', 'Alias',['class'=>'col-sm-2 control-label strongLabel']) }}
    </div>
    <div class="col-sm-10">
      {{form::text('page_alias',null,['class' =>'form-control','id'=>'page_alias'])}}
    </div>
  </div>      
  <div class="form-group">
    <div class="col-sm-2">
    {{ Form::label('page_content', 'Content',['class'=>'col-sm-2 control-label strongLabel']) }}
    </div>
    <div class="col-sm-10">
      {!! Form::textarea('page_content',null,['id'=>'summernote','required'=>'true']) !!}
    </div>
  </div>  

  

    <div class="form-group">
    <div class="col-sm-2">
    {{ Form::label('page_image', 'Page Image',['class'=>'col-sm-2 control-label strongLabel']) }}
    </div>
    <div class="col-sm-10">
      {!! Form::file('page_image',['id'=>'page_image','class'=>'btn ink-reaction btn-primary','accept'=>'image/*','onchange'=>'readURL(this)']) !!}
           
    </div>
  </div>    
    <div class="form-group">
    <div class="col-sm-2">
    {{ Form::label('page_status', 'Page Status',['class'=>'col-sm-2 control-label strongLabel']) }}
    </div>
    <div class="col-sm-10">
  {!! Form::select('page_status',['active'=>'active','dormant'=>'dormant'],null,['class'=>'form-control','required'=>'true']) !!}
    </div>
  </div>  



  <div class="card-actionbar">
    <div class="card-actionbar-row">
      {!! Form::submit('Submit',['class'=>'btn btn-flat btn-primary']) !!}
    </div>
  </div>    


{!! Form::close() !!}
</div><!--end .card-body -->
</div><!--end .card -->
@section('admin-js')

 <script src="{{ asset ('/bower_components/AdminLte/dist/js/summernote/summernote.js')}}" type="text/javascript"></script>
 <script type="text/javascript" >
  $(document).ready(function(){

  
  $('#summernote').summernote({
  height: 500,                 // set editor height
  minHeight: 500,             // set minimum height of editor
  maxHeight: 1000,             // set maximum height of editor
  focus: true                  // set focus to editable area after initializing summernote
});


  $(document).on('keyup','#page_title',function(){
    var name = $(this).val().toLowerCase().replace(/ /g,'-');
    $('#page_alias').val(name); 
  });
 
});
  
 </script>
 
 



@stop

@endsection
