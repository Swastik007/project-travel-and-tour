@extends('admin')

@section('admin-css')
<link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/custom.css') }}" />	
<style type="text/css">
	.hide{
		display: none;
	}
</style>
@stop
@section('body')

   <section class="content-header">
      <h1>
        Pages
        
      </h1>
      <ol class="breadcrumb">
      <a href="{{ URL::to('/admin/page/create') }}">
        <button type="button" class="btn btn-block btn-primary btn-flat">Add New Page</button>
       <a>
      </ol>

    </section>
   
    <section class="content">
    <br>
    <div class="box">
    <div class="box-body">
     <div class="box-header with-border">
              <h3 class="box-title">All Pages</h3>
              <div class="box">
              <div class="box-body">
                <table style="width:100%">
                    <th>Title</th>
                    <th>Alias</th>
                    <th>content</th>
                    <th>Image</th>
                    <th>Status</th>
                    <th colspan="2">Actions</th>
                  @foreach($pages as $p)
                  <tr>
                      <td>{{$p->page_title}}</td>
                      <td>{{$p->page_alias}}</td>
                      <td>{{$p->page_content}}</td>
                      <td><img style="width:50px;height:30px;"src="{{asset('/uploads/'.$p->page_image)}}" alt="Image"/></td>
                      <td><a href="{{URL::to('admin/page/changeStatus'.'/'.$p->page_id)}}">{{$p->page_status}}</a></td>
                      <td>{{ Form::open(['url'=>'admin/page/'.$p->page_id.'/edit','method'=>'GET'])}}
                          <input type="image" src="{{asset('/admin-assets/edit.png')}}"   alt="button" border="0"/>

                          {{ Form::close() }}
                      </td>
                      <td>
                          {{ Form::open(['url'=>'admin/page/'.$p->page_id,'method'=>'DELETE','id'=>'deleteForm'])}}

                          <input type="image" name="submit"   src="{{asset('/admin-assets/delete.png')}}" border="0" alt="Submit" />
                          {{ Form::close() }}
                      </td>
                  </tr>
                  @endforeach
                </table>
            </div>
            </div>
            </div>
    </section>

   


@section('admin-js')
<script type="text/javascript" src="{{ asset('/admin-assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>


<script type="text/javascript">


</script>
@stop
@endsection
