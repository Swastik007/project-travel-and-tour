@extends ('admin')

@section ('admin-css')

@stop

@section('body')
 <section class="content-header">
     
      
    </section>
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
        <div class="col-md-12">
          <!-- general form elements -->
       
            <!-- form start -->
           
            @include('partials.admin.validation')
            @include('partials.admin.flash')
             <div class="box box-success">
             <h3 class="box-title">
            <div class="box-header with-border logo">
             <i class="glyphicon glyphicon-picture"></i> <span>&nbsp;Add New Gallery&nbsp;</span></h3>
            <div class="box-body">
            @if(Request::segment(4) =='edit')
          {!!Form::model($schedules,['url'=>'/admin/gallery'.'/'.$galleries->gallery_id,'class'=>'form-horizontal', 'method'=>'PATCH','files' => 'true', 'novalidate'=>'true'])!!}
          @else
          {!!Form::open(['url'=>'/admin/gallery','class'=>'form-horizontal','method'=>'POST', 'files' => 'true', 'novalidate' => 'true'])!!}
          @endif
          <div class="form-group">
          <div class="col-sm-2">
          {{Form::label('gallery_title','Gallery Title',['class'=>'control-label strongLabel'])}}
          </div>
          <div class="col-sm-10">
          {{form::text('gallery_title',null,['class'=>'form-control','id'=>'gallery_title'])}}
        </div>
          </div>
          <div class="form-group">

          <div class="col-sm-2">
          {{Form::label('gallery_alias','Gallery Alias',['class'=>'control-label strongLabel'])}}
          </div>
          <div class="col-sm-10">
          {{form::text('gallery_alias',null,['class' =>'form-control','id'=>'gallery_alias'])}}
          </div>
          </div>



          <div class="form-group">
          

          
          <div class="col-sm-2">
          {{Form::label('gallery_image','Gallery Picture',['class'=>'control-label strongLabel'])}}
          </div>
         
          <div class="col-sm-10">

          {{Form::file('gallery_image',null,['id'=>'image','class'=>'btn btn-default btn-file','accept'=>'image/*','onchange'=>'readURL(this)'])}}
            <div  id="imageHolder" >
            </div>

          </div>
           
          </div>
          <div class="form-group">
          <div class="col-sm-2">
          {{Form::label('gallery_status','Gallery Status',['class'=>"control-label strongLabel"])}}
          </div>

          <div class="col-sm-10">
          {{Form::select('gallery_status',['active'=>'active','dormant' => 'dormant'],null,['class'=>"form-control select2 select2-hidden-accessible"])}}
          </div>
          </div>
          <div class="pull-right form-group">
          <div class="col-sm-2">
          {{form::submit('submit',null,['class'=>'form-control fa fa-plus'])}}
          </div>
          </div>

             </div>
             </div>
             

              </div>
              </div>
              </div>
              </div>
            

@section('admin-js')
<script type="text/javascript">
$(document).ready(function(){
 
//to create alias
  $(document).on('keyup','#gallery_title',function(){
    var name = $(this).val().toLowerCase().replace(/ /g,'-');
    $('#gallery_alias').val(name);



});
//image preview before uploading
function readURL(input){
  if (input.files && input.file[0]){
    var filtered= new FileReader();
    filtered.onload=function(e){
      $('#imageHolder').attr('src', e.target.result).width(200).height(200);
      };

      filtered.readAsDataURL(input.files[0]);

    
    }
  } 
});


</script>
@stop
@endsection
