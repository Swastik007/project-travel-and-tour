@extends('admin')

@section('admin-css')
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/custom.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/ratings.css') }}" />

    <style type="text/css">
        .hide{
            display: none;
        }
    </style>
@stop
@section('body')
    <section class="content-header">
        <h1>
            Add New vehicle
        </h1>
        <ol class="breadcrumb">
            <a href="{{ URL::to('/admin/vehicle') }}">
                <button type="button" class="btn btn-block btn-primary btn-flat">All vehicles</button>
                <a>
        </ol>
    </section>
    <br>
    @include('partials.admin.validation')
    @include('partials.admin.flash')

    <section>
        <div class="box">
            <div class="box-body">
                @if(Request::segment('4') == 'edit')
                    {!! Form::model($vehicle,
                      ['url' => 'admin/vehicle'.'/'.$vehicle->vehicle_id, 'class'=>'form-horizontal','method'=>'PATCH','files'=>'true','novalidate'=>'true'])!!}
                @else
                    {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/vehicle', 'class'=>'form-horizontal','files'=>'true','novalidate'=>'true'])!!}
                @endif

                <div class="form-group">
                    <div class="col-sm-2">
                        {{ Form::label('vehicle_Name', ' Name',['class'=>'col-sm-2 control-label strongLabel']) }}
                    </div>
                    <div class="col-sm-10">
                        {!! Form::text('vehicle_Name',null,['class'=>'form-control','id'=>'vehicle_Name','required'=>'true']) !!}
                    </div>
                </div>
                    <div class="form-group">
                    <div class="col-sm-2">
                        {{ Form::label('vehicle_Description', 'Content',['class'=>'col-sm-2 control-label strongLabel']) }}
                    </div>
                    <div class="col-sm-10">
                        {!! Form::textarea('vehicle_Description',null,['id'=>'summernote','required'=>'true']) !!}
                    </div>
                </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                            {{ Form::label('vehicle_Price', 'Price (NRP)',['class'=>'col-sm-2 control-label strongLabel']) }}
                        </div>
                        <div class="col-sm-10">
                            {{form::number('vehicle_Price',null,['class' =>'form-control','id'=>'vehicle_Price'])}}
                        </div>
                    </div>
                <div class="form-group">
                    <div class="col-sm-2">
                        {{ Form::label('vehicle_Status', 'vehicle Status',['class'=>'col-sm-2 control-label strongLabel']) }}
                    </div>
                    <div class="col-sm-10">
                        {!! Form::select('vehicle_Status',['active'=>'active','dormant'=>'dormant'],null,['class'=>'form-control','required'=>'true']) !!}
                    </div>
                </div>



                <div class="card-actionbar">
                    <div class="card-actionbar-row">
                        {!! Form::submit('Submit',['class'=>'btn btn-flat btn-primary']) !!}
                    </div>
                </div>


                {!! Form::close() !!}
            </div><!--end .card-body -->
        </div><!--end .card -->
    </section>

@stop