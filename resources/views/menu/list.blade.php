@extends('admin')

@section('admin-css')
<link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/custom.css') }}" />	
<style type="text/css">
	.hide{
		display: none;
	}
</style>
@stop
@section('body')

   <section class="content-header">
      <h1>
        All Menus
        
      </h1>
      <ol class="breadcrumb">
      <a href="{{ URL::to('/admin/menu/create') }}">
        <button type="button" class="btn btn-block btn-primary btn-flat">Add New Menu</button>
       <a>
      </ol>
    </section>

    


@section('admin-js')
<script type="text/javascript" src="{{ asset('/admin-assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>


<script type="text/javascript">


</script>
@stop
@endsection
