
@extends('admin')

@section('admin-css')
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/custom.css') }}" />
    <style type="text/css">
        .hide{
            display: none;
        }
    </style>
@stop
@section('body')
    <script src={{asset('/admin-assets/jquery-3.1.1.min.js')}}></script>
    <section class="content-header">
        <h1>
          Edit

        </h1>

        <ol class="breadcrumb">
            <a href="{{ URL::to('/admin/menu') }}">
                <button type="button" class="btn btn-block btn-primary btn-flat">All Menu</button>
                <a>
        </ol>
    </section>
    <br>
    <br>
    @include('partials.admin.validation')
    @include('partials.admin.flash')

    <section>
        <div class="card">
            <div class="card-body">
        @if(Request::segment('4') == 'edit')
            {!! Form::model($menu,
                ['url' => 'admin/menu'.'/'.$menu->menu_id, 'class'=>'form-horizontal','method'=>'PATCH','files'=>true]) !!}
        @else
            {!! Form::open(['url' => 'admin/menu', 'class'=>'form-horizontal','files'=>true]) !!}
        @endif

        <div class="form-group">
            <div class="col-sm-1">
                {{ Form::label('menu_title', 'Title',['class'=>'col-sm-2 control-label strongLabel']) }}
            </div>
            <div class="col-sm-9">
                {!! Form::text('menu_title',null,['class'=>'form-control','id'=>'menu_title','required'=>'true']) !!}
            </div>
        </div>
            <div class="form-group">
                <div class="col-sm-1">
                    {{ Form::label('menu_type', 'Type',['class'=>'col-sm-2 control-label strongLabel']) }}
                </div>
                <div class="col-sm-9"  >
                    {!! Form::select('menu_type',[''=>'','link'=>'Link','module'=>'Module','page' => 'Page'],null,['class'=>'form-control','id'=>'menu_type','required'=>'true','id'=>'menu_type','menu_id'=>'type']) !!}
                </div>
            </div>
            @if(Request::segment('4') == 'edit')

                    <div class="form-group hide" id="pageList">
                        <div class="col-sm-1">
                            {{ Form::label('menu_type_id', 'Page',['class'=>'col-sm-2 control-label strongLabel']) }}
                        </div>
                        <div class="col-sm-9">
                            {!! Form::select('menu_type_id',$page,null,['class'=>'form-control']) !!}

                        </div>
                    </div>
                    <div class="form-group hide" id="otherList">
                        <div class="col-sm-1">
                            {{ Form::label('menu_guid', 'Enter URL',['class'=>'col-sm-2 control-label strongLabel']) }}
                        </div>
                        <div class="col-sm-9">
                            {!! Form::text('menu_guid',null,['class'=>'form-control']) !!}

                        </div>
                    </div>

            @else
                <div class="form-group hide" id="pageList">
                    <div class="col-sm-1">
                        {{ Form::label('menu_type_id', 'Page',['class'=>'col-sm-2 control-label strongLabel']) }}
                    </div>
                  <div class="col-sm-9">
                    {!! Form::select('menu_type_id',$page,null,['class'=>'form-control']) !!}

                </div>
                </div>

                <div class="form-group hide" id="otherList">
                    <div class="col-sm-1">
                        {{ Form::label('menu_guid', 'Enter URL',['class'=>'col-sm-2 control-label strongLabel']) }}
                    </div>

                    <div class="col-sm-9">
                        {!! Form::text('menu_guid',null,['class'=>'form-control']) !!}

                    </div>
                </div>
            @endif


            <div class="form-group">
                <div class="col-sm-1">
                    {{ Form::label('', '',['class'=>'col-sm-1 control-label strongLabel']) }}
                </div>
                <div class="col-sm-1 control-label">
                    {!! Form::submit('Submit',['class'=>'btn btn-block btn-primary pull-left']) !!}

                </div>
            </div>
            {!! !Form::close() !!}
            </div>
        </div>
    </section>

    <script type="text/javascript">
        $(document).ready(function(){
            var type = $(this).val();
            console.log('changed');
            if (type == 'page') {
                $('#pageList').removeClass('hide');
                $('#otherList').addClass('hide');
                $('#menu_guid').prop('required',false);
            }else{
                $('#otherList').removeClass('hide');
                $('#pageList').addClass('hide');
                $('#menu_guid').prop('required',true);
            }
            $(document).on('change','#menu_type',function(){
                var type = $(this).val();
                console.log('changed');
                if (type == 'page') {
                    $('#pageList').removeClass('hide');
                    $('#otherList').addClass('hide');
                    $('#menu_guid').prop('required',false);
                }else{
                    $('#otherList').removeClass('hide');
                    $('#pageList').addClass('hide');
                    $('#menu_guid').prop('required',true);
                }
            });
        });

    </script>
@stop


