@extends('admin')

@section('admin-css')

@stop

@section('body')
    <section class="content-header">
        <h1>
            Iternaries:
        </h1>

        <ol class="breadcrumb">
            <a href="{{ URL::to('/admin/package') }}">
                <button type="button" class="btn btn-block btn-primary btn-flat">Back</button>
                <a>
        </ol>

    </section>

    <div ng-app="iternary" ng-controller="iternaryController" ng-init="getIternary()">
   <h2>Iternaries: </h2>
    <input type="hidden" id="id" package_id="{{$id}}"/>
        <ul>

        <li ng-repeat="i in iternaries">
            <text ng-model="i.package_iternary_day">Day @{{i.package_iternary_day}}:</text>

            <input type="text" class="form-control" ng-model="i.package_iternary_description"/>


        </li>
    </ul>
        <input type="button" ng-click="add()" value="add"/>
        <input type="submit" value="Submit" ng-click="submit()"/>

    </div>
@stop

@section('admin-js')
    <script type="text/javascript" src="{{asset('admin-assets/angular/angular.min.js')}}"></script>
    <script type="text/javascript">
        var k = angular.module('iternary',[]);
        k.controller('iternaryController',function($scope,$http){
            $scope.iternaries=[];
            var iternary={package_iternary_day:1,package_iternary_description:"Description"};
            $scope.count=1;
            $scope.getIternary=function () {
                var k = $('#id').attr('package_id');

                var url='/admin/package/alliternary/'+k;
                $http({method:'GET', url: url})
                        .success(function(response){
                            $scope.loading=false;
                            console.log(response);
                            $scope.iternaries=response;
                            $scope.count=$scope.iternaries.length;
                            console.log('count: '+$scope.count)
                        });

            }
            $scope.add=function(){
                console.log('add');
                $scope.count++;
                var iternary={package_iternary_day:$scope.count,package_iternary_description:"Description"};
                $scope.iternaries.push(iternary);
            }
            $scope.submit=function(){
                var k = $('#id').attr('package_id');
                var url='/admin/package/updateiternary/'+k;
                $http({method:'POST', url: url,data:{'iternaries':$scope.iternaries}})
                        .success(function(response){
                            console.log(response);
                            $scope.loading=false;
                            location.toSource('/admin/package');

                        });
            }
        }); </script>
@stop