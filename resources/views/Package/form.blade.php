@extends('admin')

@section('admin-css')
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/custom.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/ratings.css') }}" />

    <style type="text/css">
        .hide{
            display: none;
        }
    </style>
@stop
@section('body')
    <section class="content-header">
        <h1>
            Add New package
        </h1>
        <ol class="breadcrumb">
            <a href="{{ URL::to('/admin/package') }}">
                <button type="button" class="btn btn-block btn-primary btn-flat">All packages</button>
                <a>
        </ol>
    </section>
    <br>
    @include('partials.admin.validation')
    @include('partials.admin.flash')

    <section>
        <div class="box">
            <div class="box-body">
                @if(Request::segment('4') == 'edit')
                    {!! Form::model($package,
                      ['url' => 'admin/package'.'/'.$package->package_id, 'class'=>'form-horizontal','method'=>'PATCH','files'=>'true','novalidate'=>'true'])!!}
                @else
                    {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/package', 'class'=>'form-horizontal','files'=>'true','novalidate'=>'true'])!!}
                @endif

                <div class="form-group">
                    <div class="col-sm-2">
                        {{ Form::label('package_Name', ' Name',['class'=>'col-sm-2 control-label strongLabel']) }}
                    </div>
                    <div class="col-sm-10">
                        {!! Form::text('package_Name',null,['class'=>'form-control','id'=>'package_Name','required'=>'true']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2">
                        {{ Form::label('package_Place', 'Place',['class'=>'col-sm-2 control-label strongLabel']) }}
                    </div>
                    <div class="col-sm-10">
                        {{form::text('package_Place',null,['class' =>'form-control','id'=>'package_Place'])}}
                    </div>
                </div>
                    <div class="form-group">
                    <div class="col-sm-2">
                        {{ Form::label('package_Description', 'Content',['class'=>'col-sm-2 control-label strongLabel']) }}
                    </div>
                    <div class="col-sm-10">
                        {!! Form::textarea('package_Description',null,['id'=>'summernote','required'=>'true']) !!}
                    </div>
                </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                            {{ Form::label('package_Price', 'Price (NRP)',['class'=>'col-sm-2 control-label strongLabel']) }}
                        </div>
                        <div class="col-sm-10">
                            {{form::number('package_Price',null,['class' =>'form-control','id'=>'package_Price'])}}
                        </div>
                    </div>
                <div class="form-group">
                    <div class="col-sm-2">
                        {{ Form::label('package_Status', 'package Status',['class'=>'col-sm-2 control-label strongLabel']) }}
                    </div>
                    <div class="col-sm-10">
                        {!! Form::select('package_Status',['active'=>'active','dormant'=>'dormant'],null,['class'=>'form-control','required'=>'true']) !!}
                    </div>
                </div>



                <div class="card-actionbar">
                    <div class="card-actionbar-row">
                        {!! Form::submit('Submit',['class'=>'btn btn-flat btn-primary']) !!}
                    </div>
                </div>


                {!! Form::close() !!}
            </div><!--end .card-body -->
        </div><!--end .card -->
    </section>

@stop