@extends('admin')

@section('admin-css')
    <link type="text/css" rel="stylesheet"
          href="{{ asset('/admin-assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css') }}"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin-assets/css/custom.css') }}"/>
    <style type="text/css">
        .hide {
            display: none;
        }
    </style>
@stop
@section('body')

    <section class="content-header">
        <h1>
            All flights

        </h1>

        <ol class="breadcrumb">
            <a href="{{ URL::to('/admin/flight/create') }}">
                <button type="button" class="btn btn-block btn-primary btn-flat">Add New flight</button>
                <a>
        </ol>

    </section>
    <br>
    <br>
    <br>

    <section>
        <div class="well  col-lg-12" style="border: 5px;">

            @foreach($flights as $h)
                <div class="container jumbotron col-lg-12" style="border: 5px;">

                    <div class="col-lg-2">
                        <br>
                        <br>
                        <div class="hbox-column width-2">
                            <img style="width:100px;height:70px" class="img-circle img-responsive pull-left"
                                 src="{{ asset('/uploads/'.$h->flight_images->first()['flight_image_link'])}}" alt="image"/>
                        </div>
                    </div>

                    <div class="col-lg-9">

                        <h2 style="color: grey"><b>{{$h->flight_Name}}</b></h2>
                        <span>{{$h->flight_Location}}</span>
                        <br>
                        <span>{{$h->flight_Price}}</span>
                        <br>
                        <text>{{$h->flight_Description}}</text>
                        <br>
                        <text>{{$h->flight_Deals}}</text>
                        <br>
                        <text>{{$h->flight_Rating.' '}}  star</text>
                    </div>

                    <div class="col-lg-1">
                        <br>
                        {{ Form::open(['url'=>'admin/flight/'.$h->flight_id.'/edit','method'=>'GET'])}}
                        <input type="image" src="{{asset('/admin-assets/edit.png')}}" alt="button" border="0"/>
                        {{ Form::close() }}

                         {{ Form::open(['url'=>'admin/flight/addimage/'.$h->flight_id,'method'=>'GET'])}}
                        <input type="image" name="submit" src="{{asset('/admin-assets/addimage.png')}}" border="0"
                               alt="Submit" style="width:30px;height:30px;"/>
                        {{ Form::close() }}
                        {{ Form::open(['url'=>'admin/flight/offers/'.$h->flight_id,'method'=>'GET'])}}
                        <input type="image" name="submit" src="{{asset('/admin-assets/offer.jpg')}}" border="0"
                               alt="Submit" style="width:30px;height:30px;"/>
                        {{ Form::close() }}

                        {{ Form::open(['url'=>'admin/flight/'.$h->flight_id,'method'=>'DELETE','id'=>'deleteForm'])}}
                        <input type="image" name="submit" src="{{asset('/admin-assets/delete.png')}}" border="0"
                               alt="Submit"/>
                        {{ Form::close() }}

                        <a href="{{URL::to('admin/flight/changeStatus/'.$h->flight_id)}}">{{$h->flight_Status}}</a>
                    </div>
                    <br>

                </div>

            @endforeach


</div>


    </section>


@section('admin-js')
    <script type="text/javascript"
            src="{{ asset('/admin-assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>


@stop
@endsection
