@if(isset($errors))
	@if (count($errors) > 0)
	    @foreach ($errors->all() as $error)
	    	<div class="alert alert-success" role="alert">
				<strong> {{ $error }} </strong>
			</div>	
	    @endforeach
	@endif
@endif

