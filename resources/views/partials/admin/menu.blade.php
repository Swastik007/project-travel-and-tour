  <section class="sidebar">

                <!-- Sidebar user panel (optional) -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="{{ asset("/bower_components/AdminLte/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image" />
                    </div>
                    <div class="pull-left info">
                        <p>Swastik Thapaliya</p>
                        <!-- Status -->
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>

                <!-- search form (Optional) -->
                <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search..."/>
          <span class="input-group-btn">
            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
          </span>
                    </div>
                </form>
                <!-- /.search form -->

                <!-- Sidebar Menu -->
               <li class="active treeview">
          <a href="{{ URL::to('/admin/dashboard') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span> </i>
          </a>
          
        </li>
        <li>
        <li class="treeview-menu">
          <a href="{{ URL::to('/admin/menu') }}">
            <i class="fa fa-th"></i> <span>Menu</span>
            
          </a>
        </li>
        </li>

        <li>
        <li class="treeview-menu">
          <a href="{{ URL::to('/admin/page') }}">
            <i class="fa fa-th"></i> <span>Page</span>
            
          </a>
        </li>
        </li>


        <li>
        <li class="treeview-menu">
          <a href="{{ URL::to('/admin/gallery') }}">
            <i class="glyphicon glyphicon-picture"></i> <span>Gallery</span>
            
          </a>
        </li>
        </li>

            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
           
       