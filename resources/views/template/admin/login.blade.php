<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link type="text/css" rel="stylesheet" href="{{ asset('/bower_components/AdminLte/bootstrap/css/bootstrap.min.css') }}" />
  <link type="text/css" rel="stylesheet" href="{{ asset('/bower_components/AdminLte/dist/css/AdminLTE.min.css') }}" />

  <link type="text/css" rel="stylesheet" href="{{ asset('/bower_components/AdminLte/plugins/iCheck/square/blue.css') }}" />
   

 
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
 
  <!-- /.login-logo -->
  <div class="login-box-body">
   
   @include('partials.admin.validation')

    <form class="form floating-label" action="{{ URL::to('admin/login') }}" accept-charset="utf-8" method="post">
                {!! csrf_field() !!}
     
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Email"  id="username" name="email">
             
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <label for="username">Email</label>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" id="password" name="password">
         
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         <label for="password">Password</label>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  

    <a href="#">I forgot my password</a><br>
    <a href="#" class="text-center">Register a new membership</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="{{ asset('/bower_components/AdminLte/bootstrap/css/bootstrap.min.css') }}"></script>
<script src="{{ asset('/bower_components/AdminLte/plugins/jQuery/jQuery-2.2.3.min.js') }}"></script>

<script src="{{ asset('/bower_components/AdminLte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->


<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>