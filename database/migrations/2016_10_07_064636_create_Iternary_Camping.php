<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIternaryCamping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('camping_iternaries', function (Blueprint $table) {
            $table->increments('camping_iternary_id');
            $table->integer('camping_iternary_day');
            $table->string('camping_iternary_description');
            $table->integer('camping_id')->unsigned();
            $table->foreign('camping_id')->references('camping_id')->on('campings');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('camping_iternaries');
    }
}
