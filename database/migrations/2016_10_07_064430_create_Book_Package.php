<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookPackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_books', function (Blueprint $table) {
            $table->increments('package_book_id');
            $table->integer('package_id')->unsigned();
            $table->integer('package_bookable_id');
            $table->string('package_bookable_type');
            $table->foreign('package_id')->references('package_id')->on('packages');
            $table->enum('package_book_Status',['Confirmed','Pending','Discarded']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('package_books');
    }
}
