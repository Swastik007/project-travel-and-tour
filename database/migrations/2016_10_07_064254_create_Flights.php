<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlights extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->increments('flight_id')->unsigned();
            $table->string('flight_Name');
            $table->string('flight_From');
            $table->string('flight_To');
            $table->float('flight_Price');
            $table->string('flight_Deals');
            $table->date('flight_Departure_Date');
            $table->date('flight_Arrival_Date');
            $table->enum('flight_Status',['Active','Inactive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('flights');
    }
}
