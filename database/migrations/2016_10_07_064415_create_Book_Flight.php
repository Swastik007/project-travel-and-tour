<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookFlight extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flight_books', function (Blueprint $table) {
            $table->increments('flight_book_id');
            $table->integer('flight_id')->unsigned();
            $table->integer('flight_bookable_id');
            $table->string('flight_bookable_type');
            $table->foreign('flight_id')->references('flight_id')->on('flights');
            $table->enum('flight_book_Status',['Confirmed','Pending','Discarded']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('flights');
    }
}
