<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
   {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('page_id');
            $table->string('page_title');
            $table->string('page_alias')->unique();           
            $table->string('page_image');
            $table->text('page_content');
            $table->integer('page_created_by');           
            $table->enum('page_status', ['active','dormant']);                                
            $table->timestamps();
        });
           
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }
}
