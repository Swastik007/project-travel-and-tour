<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('activity_id')->unsigned();
            $table->string('activity_Name');
            $table->string('activity_Place');
            $table->string('activity_Type');
            $table->date('activity_Date');
            $table->float('activity_Price');
            $table->string('activity_Description');
            $table->string('activity_Deals');
            $table->enum('activity_Status',['Active','Inactive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('activities');
    }
}
