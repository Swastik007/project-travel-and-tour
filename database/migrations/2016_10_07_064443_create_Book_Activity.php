<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookActivity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_books', function (Blueprint $table) {
            $table->increments('activity_book_id');
            $table->integer('activity_id')->unsigned();
            $table->integer('activity_bookable_id');
            $table->string('activity_bookable_type');
            $table->foreign('activity_id')->references('activity_id')->on('activities');
            $table->enum('activity_book_Status',['Confirmed','Pending','Discarded']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('activity_books');
    }
}
