<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCamping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campings', function (Blueprint $table) {
            $table->increments('camping_id')->unsigned();
            $table->string('camping_Name');
            $table->string('camping_Place');
            $table->float('camping_Price');
            $table->string('camping_Description');
            $table->enum('camping_Status',['Active','Inactive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campings');
    }
}
