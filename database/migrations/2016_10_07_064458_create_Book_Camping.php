<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookCamping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('camping_books', function (Blueprint $table) {
            $table->increments('camping_book_id');
            $table->integer('camping_id')->unsigned();
            $table->integer('camping_bookable_id');
            $table->string('camping_bookable_type');
             $table->foreign('camping_id')->references('camping_id')->on('campings');
            $table->enum('camping_book_Status',['Confirmed','Pending','Discarded']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('camping_books');
    }
}
