<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageCamping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('camping_images', function (Blueprint $table) {
            $table->increments('camping_image_id');
            $table->string('camping_image_link');
            $table->integer('camping_id')->unsigned();
            $table->foreign('camping_id')->references('camping_id')->on('campings');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('camping_images');
    }
}
