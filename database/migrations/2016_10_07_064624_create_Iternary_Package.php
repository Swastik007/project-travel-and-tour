<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIternaryPackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('package_iternaries', function (Blueprint $table) {
            $table->increments('package_iternary_id');
            $table->integer('package_iternary_day');
            $table->string('package_iternary_description');
            $table->integer('package_id')->unsigned();
            $table->foreign('package_id')->references('package_id')->on('packages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('package_iternaries');
    }
}
