<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookVehicles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_books', function (Blueprint $table) {
            $table->increments('vehicle_book_id');
            $table->integer('vehicle_bookable_id');
            $table->string('vehicle_bookable_type');
            $table->integer('vehicle_id')->unsigned();
            $table->foreign('vehicle_id')->references('vehicle_id')->on('vehicles');
           $table->enum('vehicle_book_Status',['Confirmed','Pending','Discarded']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicle_books');
    }
}
