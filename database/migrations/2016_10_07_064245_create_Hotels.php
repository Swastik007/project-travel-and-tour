<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->increments('hotel_id')->unsigned();

            $table->string('hotel_Name');
            $table->string('hotel_Place');
            $table->string('hotel_Location');
            $table->float('hotel_Price');
            $table->string('hotel_Deals');
            $table->string('hotel_Description');
            $table->integer('hotel_Rating');
            $table->enum('hotel_Status',['Active','Inactive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hotels');
    }
}
