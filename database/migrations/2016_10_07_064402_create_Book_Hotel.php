<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookHotel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_books', function (Blueprint $table) {
            $table->increments('hotel_book_id');
            $table->integer('hotel_id')->unsigned();
            $table->integer('hotel_bookable_id');
            $table->string('hotel_bookable_type');
            $table->foreign('hotel_id')->references('hotel_id')->on('hotels');
            $table->enum('hotel_book_Status',['Confirmed','Pending','Discarded']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hotel_books');
    }
}
